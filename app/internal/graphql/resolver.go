//go:generate go run ../../../scripts/gqlgen.go

package graphql

import (
	"context"

	"github.com/projectunic0rn/trackq-api/internal/model"
)

// Resolver is our base resolver, that can spawn separate resolvers for different aspects of our GraphQL schema.
type Resolver struct{}

// Query returns a QueryResolver set up to respond to GraphQL mutations
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

type queryResolver struct{ *Resolver }

// Todo returns a todo list
func (r *queryResolver) Todos(ctx context.Context) ([]model.Todo, error) {
	panic("not implemented")
}
