package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"encoding/hex"
	"strings"
	"errors"
	"time"
)

type Header struct {
	Algorithm string `json:"alg"`
	Category string `json:"typ"`
}

type Payload struct {
	Subject string `json:"sub"`
	IssuedAt int64 `json:"iat"`
	Expiry int64 `json:"exp"`
}

// Encode takes pointer to Payload object and secret key and returns base64 encoded token and possible errors.
// Uses sha256 to encode tokens.
func Encode(input *Payload, secret string) (string, error) {
	if secret == "" {
		return "", errors.New("Missing secret")
	}

	tokenType := Header{"SHA256", "JWT"}
	var data [2]string

	headerSeg, err := json.Marshal(tokenType) // Header is used only to fulfill JWT requirements. ATM it's not checked or accessible from Decode function. 

	if err != nil {
		return "", errors.New("Error while marshaling headers")
	}

	payloadSeg, err := json.Marshal(input)

	if err != nil {
		return "", errors.New("Error while marshaling payload")
	}

	data[0] = base64.StdEncoding.EncodeToString(headerSeg)
	data[1] = base64.StdEncoding.EncodeToString(payloadSeg)
	dataSegment := strings.Join(data[0:2], ".")

	signature := sign(dataSegment, secret)
	hash := hex.EncodeToString(signature)
	token := dataSegment + "." + hash

	return token, nil
}

// Decode takes JWT and secret key and returns pointer to Payload object if it's signature is valid.
// If token is outdated it returns also error, but token is still aviable.
func Decode(token, secret string) (*Payload, error) {
	var payload Payload
	p := &payload

	if (!verify(token, secret)) {
		return p, errors.New("Invalid signature")
	}

	segments := strings.Split(token, ".")		
	var decodedSegments [2][]byte

	for index, element := range segments[0:2] {
		decodedElement, err := base64.StdEncoding.DecodeString(element)

		if err != nil {
			return p, errors.New("Can't decode segment as base64")
		}

		decodedSegments[index] = decodedElement
	}

	err := json.Unmarshal(decodedSegments[1], &p)

	if err != nil {
		return p, errors.New("Can't unmarshal json")
	}

	if (!p.Validate()) {
		return p, errors.New("Token not valid")
	}

	return p, nil
}

// Validate checks if Payload object is outdated, or if has invalid date-related claims.
// It's also used by Decode function.
func (p Payload) Validate() bool {
	currentUnixtime := time.Now().Unix()

	return (p.Expiry >= p.IssuedAt && p.IssuedAt >= currentUnixtime)
}

func sign(input, secret string) []byte {
	mac := hmac.New(sha256.New, []byte(secret))
	mac.Write([]byte(input))
	return mac.Sum(nil)
}

func verify(token, secret string) bool {
	segments := strings.Split(token, ".")
	
	if len(segments) != 3 {
		return false
	}

	data := strings.Join(segments[0:2], ".")
	expectedSignature := sign(data, secret)
	hash := segments[2]
	signature, err := hex.DecodeString(hash)

	if err != nil {
		return false
	}

	return hmac.Equal(signature, expectedSignature) // Used to safely check if signatures are equal, preventing from timing attacks
}