package jwt

import (
	"testing"
	"time"
	"crypto/hmac"
	"encoding/hex"
)

const sampleKey = "12123123123dadada"
const messageOne = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ"
const messageTwo = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM4OTAiLCJuYW1lIjoiSm9obmUgRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ"

func TestSign(t *testing.T) {
	shaOne := sign(messageOne, sampleKey)
	shaTwo := sign(messageTwo, sampleKey)

	if hmac.Equal(shaOne, shaTwo) {
		t.Error("Hashes are same")
	}
}

func TestVerify(t *testing.T) {
	validHash := hex.EncodeToString(sign(messageOne, sampleKey))
	validToken := messageOne + "." + validHash

	if (!verify(validToken, sampleKey)) {
		t.Error("Token should be valid")
	}

	invalidHash := hex.EncodeToString(sign(messageOne, "invalidkey"))
	invalidToken := messageOne + "." + invalidHash

	if (verify(invalidToken, sampleKey)) {
		t.Error("Token should not be valid")
	}
}

func TestPayloadValidation(t *testing.T) {
	iss := time.Now().Unix()
	exp := iss + 1000
	data := Payload{"1", iss, exp}

	if (!data.Validate()) {
		t.Errorf("Payload should be valid")
	}

	invalidExp := iss - 1
	invalidData := Payload{"1", iss, invalidExp}

	if invalidData.Validate() {
		t.Errorf("Payload should be not valid")
	}

	oldIss := iss - 1100
	oldExp := oldIss + 100
	oldData := Payload{"1", oldIss, oldExp}

	if oldData.Validate() {
		t.Errorf("Payload should be no longer valid")
	}
}

func TestIntegrationDecodeEncodedToken(t *testing.T) {
	iss := time.Now().Unix()
	exp := iss + 1000
	data := &Payload{"1", iss, exp}

	d, err := Encode(data, sampleKey)

	if err != nil {
		t.Error("Failed generating token")
	}

	output, err := Decode(d, sampleKey)

	if err != nil {
		t.Error("Failed decoding token")
	}

	if output.Subject != data.Subject {
		t.Errorf("Payload subjects were not equal, got %q, expected %q", output.Subject, data.Subject)
	}

	if output.Expiry != data.Expiry {
		t.Errorf("Payload expiry times were not equal, got %q, expected %q", output.Expiry, data.Expiry)
	}

	if output.IssuedAt != data.IssuedAt {
		t.Errorf("Payload issue times were not equal, got %q, expected %q", output.IssuedAt, data.IssuedAt)
	}
}
