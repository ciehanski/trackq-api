package model

// Todo models the todo list
type Todo struct {
	ID     string `json:"id"`
	Text   string `json:"text"`
	Done   bool   `json:"done"`
	Expire bool   `json:"expire"`
}
