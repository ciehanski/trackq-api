package main

import (
	"log"
	"net/http"

	"github.com/99designs/gqlgen/handler"
)

func main() {
	http.Handle("/", handler.Playground("GraphQL Playground", "/graphql"))
	log.Print("Goodbye, World")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
