# TrackQ API

This repo contains the code for the TrackQ backend API service. Written in GoLang with GraphQL.

[Frontend Repository](https://github.com/projectunic0rn/trackq-ui)

### Quickly get it up and running on your local machine

Run `make start` to start the application.
Run `make logs` to see the logs.
Browse to `http://localhost:8080` to send requests.
Run `make stop` when you are done.

### Docker Container

The app is currently built with 2 containers:
- **trackq-api** - the Go application.
- **track-db** - the (postgres) database.
