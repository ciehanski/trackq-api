GO_PATH=/go
APP_CONTAINER=trackq-goapp

DOCKER_COMPOSE=$(shell type -P docker-compose &>/dev/null && echo 'docker-compose' || echo 'docker-compose')

DIR:=$(shell echo `pwd`)

start: # Starts the project
	$(DOCKER_COMPOSE) up -d
stop: # Stops the project
	$(DOCKER_COMPOSE) down -v --remove-orphans
restart: # Restarts the project
	$(DOCKER_COMPOSE) restart $(APP_CONTAINER)
logs: # Show the logs from the app container if the project is running
	$(DOCKER_COMPOSE) logs -f --tail 100 $(APP_CONTAINER)
lint: # Will lint the project
	golint
	go vet ./...
	go fmt ./...
test: lint # Will run tests on the project as well as lint
	go test -v ./...
generate: # Generate gqlgen code
	docker exec -it $(APP_CONTAINER) sh -c 'go generate ./...'
build: # Rebuild the docker container
	docker build .
db-terminal: # Logs into the local PostgreSQL database
	docker exec -it trackq-db psql -d trackq trackq
fmt: # Formats code using gofmt
	docker exec -it $(APP_CONTAINER) sh -c 'gofmt -l -w -s $$(go list -f {{.Dir}} ./...)'
rungql: # Rerun GQLGen
	cd app && go run cmd/trackq/main.go -v && cd ..
help: # Prints this help
	@grep -E '^[^.]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-40s\033[0m %s\n", $$1, $$2}'